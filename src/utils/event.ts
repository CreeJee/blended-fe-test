import { ReactEventHandler, SyntheticEvent } from 'react';

type HandlerType<T extends object> = (e: T) => void;

const shallowEvent = <T>(e: SyntheticEvent<T>): object => ({ ...e });
export function throttle<GainType extends object = object>(
    handler: HandlerType<GainType>,
    getter = shallowEvent,
    timeout = 200,
): ReactEventHandler {
    let timer: number;
    return (e: SyntheticEvent) => {
        const event = getter(e) as GainType;
        if (timer > 0) {
            timer = window.setTimeout(() => {
                handler(event);
                timer = -1;
            }, timeout);
        }
    };
}
export function debounce<GainType extends object = object>(
    handler: HandlerType<GainType>,
    getter = shallowEvent,
    timeout = 200,
): ReactEventHandler {
    let timer: number;
    return (e: SyntheticEvent) => {
        const event = getter(e) as GainType;
        if (timer !== null) {
            window.clearTimeout(timer);
        }
        timer = window.setTimeout(() => {
            handler(event);
        }, timeout);
    };
}

import _Item from './Item';
import _Detail from './Detail';

export const Item = _Item;
export const Detail = _Detail;
export default {
    Item,
    Detail,
};

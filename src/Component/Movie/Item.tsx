import styled from 'styled-components/macro';
import React from 'react';
import { getStyle, Props } from '../Base/LayoutStyle';

import Title from '../Base/Components/Title';
import Desc from '../Base/Components/Desc';
import Date from '../Base/Components/Date';
import Score from '../Base/Components/Score';
import ImageView from '../Base/Components/ImageView';
import { DataReceiver } from 'Component/Base/Resolver';
import { MovieType } from 'Business/Interface/Movie';
import { IMAGE_ROOT } from 'utils/config';

type Receiver = DataReceiver<MovieType>;

const Item = styled.div<Props<'div'>>(...getStyle());
const _Default: React.FC<Receiver> = ({
    children,
    key,
    data,
    ...props
}: React.PropsWithChildren<Receiver & Props<'div'>>) => {
    const {
        id,
        release_date,
        title,
        original_title,
        overview,
        vote_average,
        poster_path,
    } = data;
    return (
        <Item
            display="block"
            boxShadow="light"
            key={key || id}
            cursor="pointer"
            {...props}
        >
            <Item position="relative">
                <Score percent={vote_average * 10} position="absolute" />
                <ImageView src={`${IMAGE_ROOT}/${poster_path}`} width="100%" />
            </Item>
            <Item>
                <Title>{title}</Title>
                <Desc>{original_title}</Desc>
                <Date>{release_date}</Date>
                <Item m="2">{overview}</Item>
            </Item>
            {children}
        </Item>
    );
};
export default _Default;

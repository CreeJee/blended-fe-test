import React from 'react';
import { getStyle, Props } from '../Base/LayoutStyle';
import styled from 'styled-components/macro';
export type Prop = Props<'section'>;
export type PropType = {} & Prop;
const style = getStyle();
const Wrapper = styled.section<Prop>(...style);
export const Desc = ({
    children,
    ...props
}: React.PropsWithChildren<PropType>) => (
    <Wrapper>{/* TODO : WRAPPING LAYOUT & divied info */}</Wrapper>
);
export default Desc;

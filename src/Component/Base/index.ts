import * as _Container from './LazyContainer';

const exportDefaults = {
    Container: _Container,
};
export const Container = _Container;
export default exportDefaults;

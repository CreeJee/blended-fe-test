import { StyledComponentProps } from 'styled-components/macro';
import {
    space,
    SpaceProps,
    layout,
    LayoutProps,
    color,
    ColorProps,
    typography,
    TypographyProps,
    border,
    BorderProps,
    shadow,
    ShadowProps,
    fontSize,
    FontSizeProps,
    grid,
    GridProps,
    Theme,
    boxShadow,
    PositionProps,
    position,
} from 'styled-system';
//일반적으로 태그가 필요하지만 기본태그를 꺼내오기에는 div로
type cssSpec = SpaceProps &
    FontSizeProps &
    LayoutProps &
    ColorProps &
    TypographyProps &
    BorderProps &
    ShadowProps &
    GridProps &
    PositionProps;
export type Props<tagName extends string> = StyledComponentProps<
    tagName,
    Theme,
    cssSpec,
    never
>;

type RestAny = Array<any>;
const extract = function(
    first: TemplateStringsArray,
    ...rest: RestAny
): [TemplateStringsArray, ...RestAny] {
    return [first, ...rest];
};
export const getStyle = () => extract`
    ${space}
    ${fontSize}
    ${layout}
    ${color}
    ${typography}
    ${border}
    ${shadow}
    ${boxShadow}
    ${position}
    ${grid}
`;

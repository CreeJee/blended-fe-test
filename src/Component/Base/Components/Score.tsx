import React from 'react';
import { getStyle, Props } from '../LayoutStyle';
import styled from 'styled-components/macro';
export type Prop = Props<'svg'>;
type TagProps = { percent: number } & Prop;
const style = getStyle();
const Wrapped = styled.svg<Prop>(...style);
export const Desc = ({
    children,
    percent,
    ...props
}: React.PropsWithChildren<TagProps>) => {
    //todo: well wrapped theme
    return (
        <Wrapped
            viewBox="0 0 36 36"
            width="50px"
            height="50px"
            backgroundColor="#fff"
            {...props}
        >
            <path
                d="M18 2.0845
                a 15.9155 15.9155 0 0 1 0 31.831
                a 15.9155 15.9155 0 0 1 0 -31.831"
                fill="none"
                stroke="#eee"
                strokeWidth="3.8"
            />
            <path
                stroke="#4cc790"
                {...{
                    strokeDasharray: `${percent}, 100`,
                    fill: 'none',
                    strokeWidth: 2.8,
                    strokeLinecap: 'round',
                    animation: 'progress 1s ease-out forwards',
                }}
                d="M18 2.0845
                a 15.9155 15.9155 0 0 1 0 31.831
                a 15.9155 15.9155 0 0 1 0 -31.831"
            />
            <text
                x="18"
                y="20.35"
                {...{
                    fill: '#000',
                    fontFamily: 'sans-serif',
                    fontSize: '0.5em',
                    textAnchor: 'middle',
                    ...props,
                }}
            >
                {percent}%
            </text>
        </Wrapped>
    );
};
export default Desc;

import React from 'react';
import { getStyle, Props } from '../LayoutStyle';
import styled from 'styled-components/macro';
export type Prop = Props<'p'>;
const style = getStyle();
const _Desc = styled.p<Props<'p'>>(...style);
export const Desc = ({ children, ...props }: React.PropsWithChildren<Prop>) => (
    <_Desc fontSize="2" m="0" ml="2" {...props}>
        {children}
    </_Desc>
);
export default Desc;

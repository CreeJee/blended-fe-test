import Desc, { Prop } from './Desc';
import React from 'react';
export const Date = ({ children, ...props }: React.PropsWithChildren<Prop>) => (
    <Desc fontSize="1" color="gray" m="0" ml="2" {...props}>
        {children}
    </Desc>
);
export default Date;

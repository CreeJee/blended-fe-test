import React from 'react';
import { getStyle, Props } from '../LayoutStyle';
import styled from 'styled-components/macro';
export type Prop = Props<'img'>;
const style = getStyle();
const Img = styled.img<Props<'img'>>(...style);
export const ImageView = ({
    children,
    ...props
}: React.PropsWithChildren<Prop>) => (
    <>
        {children}
        <Img loading="lazy" {...props} />
    </>
);
export default ImageView;

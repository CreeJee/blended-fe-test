import React from 'react';
import { getStyle, Props } from '../LayoutStyle';
import styled from 'styled-components/macro';
const _Title = styled.h3<Props<'h3'>>(...getStyle());
export const Title = ({
    children,
    ...props
}: React.PropsWithChildren<Props<'h3'>>) => {
    return (
        <_Title fontSize="4" ml="2" {...props}>
            {children}
        </_Title>
    );
};
export default Title;

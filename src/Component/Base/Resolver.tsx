import { PropsWithChildren } from 'react';
import React from 'react';

export type PropsReceiver<T> = {
    component: React.FC<DataReceiver<T>>;
};
export type DataReceiver<T> = {
    data: T;
};
export function DataResolver<T>({
    component,
    data,
    children,
    ...props
}: PropsWithChildren<PropsReceiver<T> & DataReceiver<T>>) {
    return component({ data, children, ...props });
}

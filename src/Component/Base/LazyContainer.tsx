import styled from 'styled-components/macro';
import { getStyle, Props } from './LayoutStyle';
import React, { useState, useLayoutEffect } from 'react';
import { debounce } from 'utils/event';
import { SetStateType } from 'utils/type';
import { DataResolver, DataReceiver } from './Resolver';
// overflow 범위 재구성
type Prop = Props<'section'>;
type RenderType<DataType extends object> = {
    page: number;
    get: (page: number) => Promise<DataType[]>;
    render: React.FC<DataReceiver<DataType>>;
};
type PropParam<DataType extends object> = Prop & RenderType<DataType>;
const _Container = styled.section<Prop>(...getStyle());
const renderCycle = async function<DataType extends object>(
    { page, get, render }: RenderType<DataType>,
    views: JSX.Element[],
) {
    const viewSize = views.length;
    const clonedView = [...views];
    try {
        const response = await get(page);
        for (let index = 0; index < response.length; index++) {
            const data = response[index];
            const current = (
                <DataResolver
                    component={render}
                    data={data}
                    key={index + viewSize}
                />
            );
            if (current !== null) {
                clonedView.push(current);
            }
        }
    } catch (e) {
        views.splice(0);
    }
    return clonedView;
};
const onScroll = <DataType extends object>(
    props: RenderType<DataType>,
    [views, setView]: [JSX.Element[], SetStateType<JSX.Element[]>],
) => {
    return debounce<{ target: HTMLElement }>(
        ({ target }) => {
            const scrollEnd =
                target.scrollHeight - (target.scrollTop + target.offsetHeight) <
                100;
            if (scrollEnd) {
                renderCycle<DataType>(props, views).then(setView);
            }
        },
        ({ target }) => ({ target }),
    );
};
export const Container = <DataType extends object = object>({
    children,
    get,
    page,
    render,
    ...props
}: React.PropsWithChildren<PropParam<DataType>>) => {
    const [views, setView] = useState<JSX.Element[]>([]);
    const renderProps = { get, page, render };
    const scrollHandler = onScroll<DataType>(renderProps, [views, setView]);
    useLayoutEffect(() => {
        async function runEffect() {
            const mergedViews = await renderCycle<DataType>(renderProps, views);
            setView(mergedViews);
        }
        runEffect();
        return () => setView([]);
    }, []);
    return (
        <_Container
            {...{
                display: 'grid',
                gridTemplateColumns: ['1fr', 'repeat(3, 1fr)'],
                gridAutoRows: 'minmax(min-content, max-content)',
                gridGap: '10px',
                height: '100%',
                ...props,
                onScroll: scrollHandler,
                overflowY: 'scroll',
            }}
        >
            {views}
        </_Container>
    );
};
export default Container;

import React from 'react';
import App from '../App';

import AppProvider from '../Component/AppProvider';
export default { title: 'App' };

export const View = () => {
    return (
        <AppProvider>
            <App />
        </AppProvider>
    );
};
export const NoTheme = () => {
    return <App />;
};

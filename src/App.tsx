import React from 'react';
import { MovieList } from 'Business/Component/MovieList';
// import Container from 'components/Base/LazyContainer';
// import Item from 'components/Movie/Item';

const App = () => {
    return (
        <>
            <header>
                <nav>
                    <ul>
                        <li>
                            <a href="#">1</a>
                        </li>
                        <li>
                            <a href="#">2</a>
                        </li>
                        <li>
                            <a href="#">3</a>
                        </li>
                    </ul>
                </nav>
            </header>
            <MovieList />
        </>
    );
};

export default App;

import React, { useState } from 'react';
import { MovieType, MovieRow } from 'Business/Interface/Movie';
import { API_ROOT, API_KEY } from 'utils/config';
import AppProvider from 'Component/AppProvider';
import Container from 'Component/Base/LazyContainer';
import { Item } from 'Component/Movie';

export const MovieList = () => {
    const [current, setPage] = useState(1);

    async function getMovieList(page: number): Promise<MovieType[]> {
        const row: MovieRow = await (
            await fetch(
                `${API_ROOT}/3/movie/popular?api_key=${API_KEY}&language=en-US&page=${page}`,
            )
        ).json();
        setPage(current + 1);
        return row.results;
    }
    return (
        <AppProvider>
            <Container<MovieType>
                render={Item}
                get={getMovieList}
                page={current}
            />
        </AppProvider>
    );
};
